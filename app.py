from flask import Flask
from src.ec.gob.sercop.apisNoSql.controller.AuthenticationController import init as init_authentication
from src.ec.gob.sercop.apisNoSql.controller.TeSuperciasKardexAccionistasController import init as init_tesupercias_kardex
from flask_cors import CORS


def create_app():
    app = Flask(__name__)
    CORS(app, support_credentials=True)
    init_authentication(app)
    init_tesupercias_kardex(app)
    return app

if __name__ == '__main__':
    app = create_app()
    app.run(host='0.0.0.0',port=8088, debug=True)