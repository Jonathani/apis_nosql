from datetime import datetime
from src.ec.gob.sercop.apisNoSql.entities.SuperIntendencia import SuperIntendencia
from json import loads
from src.ec.gob.sercop.apisNoSql.utils.Variables import Variables
import pandas as pd


class SuperIntendenciaServices():

    @staticmethod
    def listar_te_supercias_kardex_acc(supercias_kardex_dto):

        lista_total = []
        lista_de_identificacion = []

        datetime_object = datetime.strptime(supercias_kardex_dto['fecha'], '%Y-%m-%d %H:%M:%S')

        if (supercias_kardex_dto["tipo_consulta"] == Variables.TIPO_CONSULTA_EN ): #Consulta En
            for row in supercias_kardex_dto["rucs"].split(","):
                lista_de_identificacion.append(row)
            for row in supercias_kardex_dto["cedulas"].split(","):
                lista_de_identificacion.append(row)

            query = TeSuperciasKardexAccionistas.objects.filter(cedula__in=lista_de_identificacion)
        elif (supercias_kardex_dto["tipo_consulta"] == Variables.TIPO_CONSULTA_DE ):#Consulta De
            for row in supercias_kardex_dto["rucs"].split(","):
                lista_de_identificacion.append(row)

            query = TeSuperciasKardexAccionistas.objects.filter(ruc__in=lista_de_identificacion)

        
        df_kardex_accionistas = pd.DataFrame()
        for row in query:
            row_insert = {
                'exp_acc': f"{row.expediente}{row.cedula}",
                'expediente': row.expediente,
                'ruc': row.ruc,
                'cedula': row.cedula,
                'nombre': row.nombre,
                'tipo_de_transaccion': row.tipo_de_transaccion,
                'valor': row.valor,
                'fecha_registro': row.fecha_registro
            }
            df_new_row = pd.DataFrame(row_insert, index=[0])            
            df_kardex_accionistas = pd.concat([df_kardex_accionistas, df_new_row], ignore_index = True)
        print("pasa kardex")

        #Fecha máxima
        df_fechas_max = pd.DataFrame()
        df_fechas_max = df_kardex_accionistas[['exp_acc','expediente','cedula','nombre','fecha_registro']]
        df_fechas_max = df_fechas_max[df_fechas_max['fecha_registro'].notnull()].copy()
        df_fechas_max['fecha_registro'] = pd.to_datetime(df_fechas_max['fecha_registro'], format='%Y-%m-%d %H:%M:%S')
        # df_fechas_max['fecha_registro'] = df_fechas_max['fecha_registro'].astype('datetime64[ns]')
        df_fechas_max = df_fechas_max.loc[df_fechas_max['fecha_registro'] <= datetime_object]
        df_fechas_max = df_fechas_max.groupby(['exp_acc','expediente','cedula','nombre'], as_index=False)['fecha_registro'].max()
        df_fechas_max = df_fechas_max.drop_duplicates()
        print("pasa fecha maxima")

        #Accionistas_1
        df_accionistas_1 = pd.DataFrame()
        df_kardex_accionistas['fecha_registro'] = pd.to_datetime(df_kardex_accionistas['fecha_registro'], format='%Y-%m-%d %H:%M:%S')
        df_accionistas_1 = pd.merge(df_kardex_accionistas, df_fechas_max, left_on=['expediente','cedula', 'fecha_registro'], right_on=['expediente','cedula', 'fecha_registro'], how='inner')
        df_accionistas_1['fecha_registro'] = pd.to_datetime(df_accionistas_1['fecha_registro'], format='%Y-%m-%d %H:%M:%S')
        df_accionistas_1 = df_accionistas_1[['exp_acc_x', 'expediente', 'ruc', 'cedula', 'nombre_x', 'tipo_de_transaccion', 'valor', 'fecha_registro']]
        df_accionistas_1 = df_accionistas_1.rename(columns={'exp_acc_x': 'exp_acc', 'nombre_x': 'nombre'})
        df_accionistas_1 = df_accionistas_1.loc[~df_accionistas_1['tipo_de_transaccion'].isin(['venta','retiro del capital'])]
        df_accionistas_1['fecha_registro'] = pd.to_datetime(df_accionistas_1['fecha_registro'], format='%Y-%m-%d %H:%M:%S')
        df_accionistas_1 = df_accionistas_1.drop_duplicates()
        print("pasa accionistas")
        
        #Ventas        
        list_aAccionistas_1_exp_acc = []
        for ind in df_accionistas_1.index:
            list_aAccionistas_1_exp_acc.append(df_accionistas_1['exp_acc'][ind])

        list_df_fechas_max_exp_acc = []
        for ind in df_fechas_max.index:
            list_df_fechas_max_exp_acc.append(df_fechas_max['exp_acc'][ind])

        df_ventas = pd.DataFrame()
        df_ventas = df_kardex_accionistas.loc[~df_kardex_accionistas['exp_acc'].isin(list_aAccionistas_1_exp_acc)]
        df_ventas = df_ventas.loc[df_ventas['exp_acc'].isin(list_df_fechas_max_exp_acc)]
        #df_ventas['fecha_registro'] = pd.to_datetime(df_ventas['fecha_registro'], format='%Y-%m-%d %H:%M:%S')
        df_ventas['fecha_registro'] = df_ventas['fecha_registro'].astype('datetime64[ns]')
        df_ventas = df_ventas.loc[df_ventas['fecha_registro'] <= datetime_object]
        # df_ventas['valor'] = pd.to_numeric(df_ventas['valor'])
        df_ventas['valor'] = df_ventas['valor'].astype(float)

        #Suma Acciones
        df_suma_acciones = pd.DataFrame()        
        df_suma_acciones = df_ventas.groupby(['exp_acc','expediente','ruc','cedula','nombre'], as_index=False)['valor'].sum()
        df_suma_acciones['valor'] = df_suma_acciones['valor'].astype(float)
        df_suma_acciones['valor'] = df_suma_acciones['valor'].round(4)
        
        print("pasa suma acciones")

        #Total
        df_total = pd.DataFrame()        
        df_suma_acciones = df_suma_acciones.loc[df_suma_acciones['valor'] > 0]
        df_suma_acciones = df_suma_acciones[['expediente', 'ruc', 'cedula', 'nombre']]        
        df_total = pd.concat([df_total, df_suma_acciones], ignore_index = True)
        
        df_accionistas_1 = df_accionistas_1[['expediente', 'ruc', 'cedula', 'nombre']]
        df_total = pd.concat([df_total, df_accionistas_1], ignore_index = True)
        df_total = df_total.drop_duplicates()
        print("pasa accionistas")

        #Parsear el resultado como json
        result = df_total.to_json(orient="records")
        lista_total = loads(result)

        nombre_archivo = "pruebaTotalConsultaDe.xlsx"
        df_total.to_excel(nombre_archivo)

        return lista_total
        
