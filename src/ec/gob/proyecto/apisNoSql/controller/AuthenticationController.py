from flask import request, jsonify
from src.ec.gob.sercop.apisNoSql.services.JwtHandler import generate_jwt
from src.ec.gob.sercop.apisNoSql.services.AuthProvider import authenticate

from flask_cors import cross_origin
import logging
import datetime
def init(app):

    @app.route('/api/apisNoSql/authentication', methods=['POST'])
    @cross_origin(supports_credentials=True)
    def auth():
        user = request.json.get('user')
        password = request.json.get('password')
        if not user or not password:
            return jsonify({"message": "User or password missing", "status": 400}), 400

        user_data = authenticate(user, password)
        if not user_data:
            return jsonify({"message": "Invalid credentials", "status": 400}), 400

        token = generate_jwt(payload=user_data, lifetime=60) # <--- generates a JWT with valid within 1 hour by now
        return jsonify({"token": token.decode(), "status": 200}), 200