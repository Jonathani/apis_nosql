from flask import jsonify, request
# from services.auth_guard import auth_guard
from flask_cors import cross_origin
from src.ec.gob.sercop.apisNoSql.services.AuthGuard import auth_guard
from src.ec.gob.sercop.apisNoSql.services.SuperIntenciaControllerServices import SuperIntenciaControllerServices

def init(app):

    @app.route('/api/teSuperciasKardexAccController/superciasKardex', methods=['POST'])
    @auth_guard()
    @cross_origin(supports_credentials=True)
    def supercias_kardex():
        rucs = request.json.get('rucs').replace("'","")
        cedulas = request.json.get('cedulas').replace("'","")
        fecha = request.json.get('fecha').replace("'","").replace(".000","")
        tipo_consulta = request.json.get('tipoConsulta')
        try:
            if (len(fecha.strip()) != 0):
                supercias_kardex_dto = {
                    'rucs': rucs,
                    'cedulas': cedulas,
                    'fecha': fecha,
                    'tipo_consulta': tipo_consulta
                }
                respuesta_consulta = TeSuperciasKardexAccionistasServices.listar_te_supercias_kardex_acc(supercias_kardex_dto)
                mensaje = f"Correcto"
                estado = True
                respuesta = {"status": estado,"mensaje": mensaje, "consulta": respuesta_consulta}
                response = 200
            else:
                mensaje = f"Estimado Usuario, debe ingresar todos los datos para continuar con el proceso"
                estado = False
                respuesta = {"status": estado, "mensaje": mensaje}
                response = 500
        except Exception as e:
            respuesta = {"respuesta": e}
            response = 500

        return jsonify(respuesta), response
