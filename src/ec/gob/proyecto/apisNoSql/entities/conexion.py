import tomli
from mongoengine import Document, StringField, IntField, DateField, DateTimeField, connect

archivo_configuracion = tomli.load(open(r"configuracion.toml", mode="rb"))

#Conexion a las bdd NOSQL -> MongoDB
connect(
    alias = archivo_configuracion['mongodb']['bdd_alias_mongodb'],
    db = archivo_configuracion['mongodb']['bdd_mongodb'],
    host=archivo_configuracion['mongodb']['host_mongodb'],
    port= archivo_configuracion['mongodb']['puerto_mongodb'],
    username= archivo_configuracion['mongodb']['user_mongodb'],
    password= archivo_configuracion['mongodb']['pass_mongodb'],    
)