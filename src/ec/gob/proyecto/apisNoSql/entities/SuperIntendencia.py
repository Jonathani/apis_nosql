from src.ec.gob.sercop.apisNoSql.entities.conexion import Document, IntField, StringField, archivo_configuracion

class SuperIntendencia(Document):
    _id = IntField(required=True)
    expediente = StringField()
    ruc = StringField()
    cedula = StringField()
    nombre = StringField()
    tipo_de_transaccion = StringField()
    fecha_resolucion = StringField()
    tipo_de_inversion = StringField()
    valor = StringField()
    fecha_registro = StringField()
    pais = StringField()
    fecha_act = StringField()

    def to_dict(self):
        return {
            'expediente': self.expediente,
            'ruc': self.ruc,
            'cedula': self.cedula,
            'nombre': self.nombre,
            'tipo_de_transaccion': self.tipo_de_transaccion,
            'fecha_resolucion': self.fecha_resolucion,
            'tipo_de_inversion': self.tipo_de_inversion,
            'valor': self.valor,
            'fecha_registro': self.fecha_registro,
            'pais': self.pais,
            'fecha_act': self.fecha_act,
        }    
   
    meta = {'db_alias': archivo_configuracion['mongodb']['bdd_alias_mongodb'], 'collection': archivo_configuracion['mongodb']['collection_mongodb']}
